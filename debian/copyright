Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: yg.lockfile
Source: https://github.com/yougov/yg.lockfile/

Files: *
Copyright: 2012-2017, Jason R. Coombs <jaraco@jaraco.com>
License: Expat
Comment:
 * License grant
   Upstream lists the license of its code as part of the classifiers listed in
   the setup.py file, which in particular for this project states:
    "License :: OSI Approved :: MIT License",
   This is discussed in the issue:
    https://github.com/jaraco/skeleton/issues/1
   "In many of the projects I maintain, I'm asked to create and maintain a
   license file separate from the license declaration in the metadata. I'd
   rather not paste a license file into the source, maintain the license
   declaration separately, and ensure that those stay in sync. In fact many
   times I've seen them fall out of sync. I'd like instead to have a single
   point where the license is referenced and let that serve as the
   authoritative indication of the license under which the project is
   released.
   .
   To communicate this, I'm filing this ticket with the project skeleton from
   which many of the projects I maintain is derived."
 * Copyright assignment
   There are no copyright claims in the source tree, we are using the author
   information from the setup.py and the years of the copyright from the git
   log history.

Files: debian/*
Copyright: 2017, Maximiliano Curia <maxy@debian.org>
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
